package jyt.geconomicus.helper;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class GlobalStatsFrame extends JFrame
{
	private static class StatPanel extends JPanel
	{
		private List<StatPoint> mPoints;

		public StatPanel(List<StatPoint> pPoints)
		{
			super();
			mPoints = pPoints;
		}
	}

	private static class StatPoint
	{
		double x;
		double y;
		public StatPoint(double pX, double pY)
		{
			super();
			x = pX;
			y = pY;
		}
	}
	public GlobalStatsFrame(List<Game> pGames)
	{
		List<StatPoint> libreCurrOptimizers = new ArrayList<>();
		for (Game game : pGames)
		{
			// compute libre currency optimizers
		}
	}
}
