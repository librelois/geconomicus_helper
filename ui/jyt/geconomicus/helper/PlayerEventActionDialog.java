package jyt.geconomicus.helper;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import jyt.geconomicus.helper.Event.EventType;
import jyt.geconomicus.helper.HelperUI.ActionCommand;

/**
 * A multi-purpose dialog to gather information on the assets of a player - money or cards.
 * @author jytou
 */
public class PlayerEventActionDialog extends JDialog
{
	private static final String ESCAPE_ACTION = "escape"; //$NON-NLS-1$
	private static final String ENTER_ACTION = "enter"; //$NON-NLS-1$

	/**
	 * In the case when we are using coins of value 3 in libre money, verify that the number of weak coins correspond to the number of coins the player had last turn
	 * @author jytou
	 */
	protected class NbCoinVerifier implements DocumentListener
	{
		private void testValue()
		{
			try
			{
				final int nbWeakCoins = Integer.parseInt(mWeakCoinsTF.getText());
				if (nbWeakCoins == mOldWeakCoins13)
				{
					try
					{
						final int nbStrongCoins = Integer.parseInt(mStrongCoinsTF.getText());
						final int newSum = EventType.DEATH.equals(mOriginalEvent) ? 4 : EventType.QUIT.equals(mOriginalEvent) ? 0 : (nbWeakCoins + 3 * nbStrongCoins) / 2 + 4;
						final int newStrong = newSum / 3;
						final int newWeak = newSum - newStrong * 3;
						mLabelCorrectOldWeakCoins.setText("<html><font color=#008800>" + MessageFormat.format(UIMessages.getString("CreditActionDialog.Label.OKCoins2Give"), newStrong, newWeak) + "</font></html>"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					}
					catch (NumberFormatException e2)
					{
						mLabelCorrectOldWeakCoins.setText("<html><font color=#008800>" + MessageFormat.format(UIMessages.getString("CreditActionDialog.Label.OKCoins2Give"), "?", "?") + "</font></html>"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
					}
					return;
				}
			}
			catch (NumberFormatException e)
			{
			}
			mLabelCorrectOldWeakCoins.setText("<html><font color=#FF0000>" + MessageFormat.format(UIMessages.getString("CreditActionDialog.Label.WarningWrongWeakCoins"), mOldWeakCoins13) + "</font></html>"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}

		@Override
		public void removeUpdate(DocumentEvent pE)
		{
			testValue();
		}

		@Override
		public void insertUpdate(DocumentEvent pE)
		{
			testValue();
		}

		@Override
		public void changedUpdate(DocumentEvent pE)
		{
			testValue();
		}
	}

	private class CancelAction extends AbstractAction implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent pEvent)
		{
			setVisible(false);
		}
	}

	// The different purposes that this dialog can serve
	public enum Purpose
	{
		MONEY_MASS_CHANGE,            // money mass change only
		BANK_INVESTMENT,              // the bank invests some of its seized assets (interest gained or cards seized)
		NEW_OR_REIMB_CREDIT,          // a player takes or pays back a credit
		DEFAULT,                      // a player is defaulting on his debts
		PLAYER_ASSESSMENT_DEBT_MONEY, // assessment of a player in the debt-money system
		PLAYER_FINAL_ASSESSMENT_LIBRE_MONEY,// assessment of a player in the libre currency system
		PLAYER_TURN_ASSESSMENT_LIBRE_MONEY
	};

	// A helper to tell for which purposes we need to show cards information in the dialog
	private final static Set<Purpose> SHOW_CARDS_PURPOSE = new HashSet<>();
	static
	{
		SHOW_CARDS_PURPOSE.add(Purpose.BANK_INVESTMENT);
		SHOW_CARDS_PURPOSE.add(Purpose.DEFAULT);
		SHOW_CARDS_PURPOSE.add(Purpose.PLAYER_ASSESSMENT_DEBT_MONEY);
		SHOW_CARDS_PURPOSE.add(Purpose.PLAYER_FINAL_ASSESSMENT_LIBRE_MONEY);
	}

	// If != -1, we are using the 1-3 system in libre money
	private int mOldWeakCoins13;

	private EventType mOriginalEvent;

	// Text fields
	private JTextField mPrincipalTF;
	private JTextField mInterestTF;
	private JTextField mWeakCoinsTF;
	private JTextField mMediumCoinsTF;
	private JTextField mStrongCoinsTF;
	private JTextField mWeakCardsTF;
	private JTextField mMediumCardsTF;
	private JTextField mStrongCardsTF;
	// Return values
	private EventType mEventType = null;
	private String mPrincipalLabel;
	private int mPrincipal = 0;
	private int mInterest = 0;
	private int mWeakCoins = 0;
	private int mMediumCoins = 0;
	private int mStrongCoins = 0;
	private int mWeakCards = 0;
	private int mMediumCards = 0;
	private int mStrongCards = 0;
	private boolean mApplied = false;
	private JLabel mLabelCorrectOldWeakCoins;
	private JTextField mComponentWithFocus;

	private class ApplyAction extends AbstractAction implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent pEvent)
		{
			if (mPrincipalTF != null)
				try
				{
					mPrincipal = Integer.valueOf(mPrincipalTF.getText());
				}
				catch (NumberFormatException e)
				{
					showErrorMessage(UIMessageKeyProvider.GENERAL_CREDIT_PRINCIPAL.getMessage());
					return;
				}
			if (mInterestTF != null)
			{
				try
				{
					mInterest = Integer.valueOf(mInterestTF.getText());
				}
				catch (NumberFormatException e)
				{
					showErrorMessage(UIMessageKeyProvider.GENERAL_CREDIT_INTEREST.getMessage());
					return;
				}
			}
			if (mWeakCardsTF != null)
			{
				try
				{
					mWeakCards = Integer.valueOf(mWeakCardsTF.getText());
				}
				catch (NumberFormatException e)
				{
					showErrorMessage(UIMessageKeyProvider.GENERAL_CARDS_WEAK.getMessage());
					return;
				}
				try
				{
					mMediumCards = Integer.valueOf(mMediumCardsTF.getText());
				}
				catch (NumberFormatException e)
				{
					showErrorMessage(UIMessageKeyProvider.GENERAL_CARDS_MEDIUM.getMessage());
					return;
				}
				try
				{
					mStrongCards = Integer.valueOf(mStrongCardsTF.getText());
				}
				catch (NumberFormatException e)
				{
					showErrorMessage(UIMessageKeyProvider.GENERAL_CARDS_STRONG.getMessage());
					return;
				}
			}
			if (mWeakCoinsTF != null)
			{
				try
				{
					mWeakCoins = Integer.valueOf(mWeakCoinsTF.getText());
				}
				catch (NumberFormatException e)
				{
					showErrorMessage(UIMessageKeyProvider.GENERAL_MONEY_WEAK.getMessage());
					return;
				}
				if (mOldWeakCoins13 != -1)
					mMediumCoins = 0;
				else
					try
					{
						mMediumCoins = Integer.valueOf(mMediumCoinsTF.getText());
					}
					catch (NumberFormatException e)
					{
						showErrorMessage(UIMessageKeyProvider.GENERAL_MONEY_MEDIUM.getMessage());
						return;
					}
				try
				{
					mStrongCoins = Integer.valueOf(mStrongCoinsTF.getText());
				}
				catch (NumberFormatException e)
				{
					showErrorMessage(UIMessageKeyProvider.GENERAL_MONEY_STRONG.getMessage());
					return;
				}
			}
			mApplied = true;
			setVisible(false);
		}

		public void showErrorMessage(String pField)
		{
			JOptionPane.showMessageDialog(mPrincipalTF, MessageFormat.format(UIMessages.getString("CreditActionDialog.Error.Message.ValueForFieldNotANumber"), pField), UIMessages.getString("CreditActionDialog.Error.Title.InputANumber"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	public PlayerEventActionDialog(final JFrame pParent, final String pTitle, final Purpose pPurpose, final EventType pOriginalEvent)
	{
		this(pParent, pTitle, 3, 1, pPurpose, pOriginalEvent, -1);
	}

	public PlayerEventActionDialog(final JFrame pParent, final String pTitle, final int pDefaultPrincipal, final int pDefaultInterest, final Purpose pPurpose, EventType pOriginalEvent, int pOldWeakCoins)
	{
		super(pParent, pTitle);
		mOriginalEvent = pOriginalEvent;
		mOldWeakCoins13 = pOldWeakCoins;
		int width = 600;
		int height = 250;
		setModal(true);
		final JPanel mainPanel = new JPanel(new GridBagLayout());
		switch (pPurpose)
		{
		case NEW_OR_REIMB_CREDIT:
			width = 400;
			height = 150;
			mPrincipalLabel = UIMessageKeyProvider.GENERAL_CREDIT_PRINCIPAL.getMessage();
			break;
		case DEFAULT:
			width = 700;
			height = 300;
			mPrincipalLabel = UIMessageKeyProvider.GENERAL_CREDIT_PRINCIPAL.getMessage();
			break;
		case MONEY_MASS_CHANGE:
			width = 450;
			height = 150;
			mPrincipalLabel = UIMessages.getString("CreditActionDialog.Label.MoneyMassDelta"); //$NON-NLS-1$
			break;
		case BANK_INVESTMENT:
			width = 500;
			height = 200;
			mPrincipalLabel = UIMessages.getString("CreditActionDialog.Label.InvestedMoney"); //$NON-NLS-1$
			break;
		case PLAYER_ASSESSMENT_DEBT_MONEY:
			width = 450;
			height = 200;
			mPrincipalLabel = UIMessages.getString("CreditActionDialog.Label.RemainingMoney"); //$NON-NLS-1$
			break;
		default:
			break;
		}
		setSize(width, height);
		setLocation(pParent.getX() + pParent.getWidth() / 2 - width / 2, pParent.getY() + pParent.getHeight() / 2 - height / 2);
		int y = 0;
		if (!(Purpose.PLAYER_FINAL_ASSESSMENT_LIBRE_MONEY.equals(pPurpose) || Purpose.PLAYER_TURN_ASSESSMENT_LIBRE_MONEY.equals(pPurpose)))
		{
			mPrincipalTF = createField(mainPanel, y++, mPrincipalLabel);
			mPrincipalTF.setText(String.valueOf(pDefaultPrincipal));
			mPrincipalTF.requestFocusInWindow();
		}
		if (Purpose.DEFAULT.equals(pPurpose) || Purpose.NEW_OR_REIMB_CREDIT.equals(pPurpose))
		{
			mInterestTF = createField(mainPanel, y++, UIMessageKeyProvider.GENERAL_CREDIT_INTEREST.getMessage());
			mInterestTF.setText(String.valueOf(pDefaultInterest));
		}
		if (Purpose.PLAYER_FINAL_ASSESSMENT_LIBRE_MONEY.equals(pPurpose) || Purpose.PLAYER_TURN_ASSESSMENT_LIBRE_MONEY.equals(pPurpose))
		{
			mWeakCoinsTF = createField(mainPanel, y++, UIMessageKeyProvider.GENERAL_MONEY_WEAK.getMessage());
			if (mOldWeakCoins13 == -1)
			{
				mMediumCoinsTF = createField(mainPanel, y++, UIMessageKeyProvider.GENERAL_MONEY_MEDIUM.getMessage());
				mComponentWithFocus = mWeakCoinsTF;
			}
			mStrongCoinsTF = createField(mainPanel, y++, UIMessageKeyProvider.GENERAL_MONEY_STRONG.getMessage());
			if (mOldWeakCoins13 != -1)
			{
				mComponentWithFocus = mStrongCoinsTF;
				mStrongCoinsTF.setText(""); //$NON-NLS-1$
				mStrongCoinsTF.getDocument().addDocumentListener(new NbCoinVerifier());

				mLabelCorrectOldWeakCoins = new JLabel("<html><center><font color=#aa0000>" + UIMessages.getString("CreditActionDialog.Label.Default3Coins") + "</font></center></html>"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				mWeakCoinsTF.setText(String.valueOf(mOldWeakCoins13));
				mainPanel.add(mLabelCorrectOldWeakCoins, new GridBagConstraints(0, y++, 2, 1, 1, 0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(20, 10, 0, 0), 10, 0));
				mWeakCoinsTF.getDocument().addDocumentListener(new NbCoinVerifier());
			}
		}
		if (SHOW_CARDS_PURPOSE.contains(pPurpose))
		{
			mWeakCardsTF = createField(mainPanel, y++, UIMessageKeyProvider.GENERAL_CARDS_WEAK.getMessage());
			mMediumCardsTF = createField(mainPanel, y++, UIMessageKeyProvider.GENERAL_CARDS_MEDIUM.getMessage());
			mStrongCardsTF = createField(mainPanel, y++, UIMessageKeyProvider.GENERAL_CARDS_STRONG.getMessage());
		}
		if (Purpose.DEFAULT.equals(pPurpose))
		{
			final ActionListener rbActionListener = new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent pEvent)
				{
					if (ActionCommand.ACTION_CANNOT_PAY.getMnemo().equals(pEvent.getActionCommand()))
						mEventType = EventType.CANNOT_PAY;
					else if (ActionCommand.ACTION_BANKRUPTCY.getMnemo().equals(pEvent.getActionCommand()))
						mEventType = EventType.BANKRUPT;
					else if (ActionCommand.ACTION_PRISON.getMnemo().equals(pEvent.getActionCommand()))
						mEventType = EventType.PRISON;
				}
			};
			final JPanel statePlayerPanel = new JPanel(new GridBagLayout());
			final JRadioButton cannotPayRB = createRadio(statePlayerPanel, 0, UIMessages.getString("CreditActionDialog.Option.Label.SeizedOk"), ActionCommand.ACTION_CANNOT_PAY.getMnemo(), rbActionListener); //$NON-NLS-1$
			final JRadioButton bankruptRB = createRadio(statePlayerPanel, 1, UIMessages.getString("CreditActionDialog.Option.Label.SeizedBankrupt"), ActionCommand.ACTION_BANKRUPTCY.getMnemo(), rbActionListener); //$NON-NLS-1$
			final JRadioButton prisonRB = createRadio(statePlayerPanel, 2, UIMessages.getString("CreditActionDialog.Option.Label.SeizedPrison"), ActionCommand.ACTION_PRISON.getMnemo(), rbActionListener); //$NON-NLS-1$
			statePlayerPanel.setBorder(new TitledBorder(new BevelBorder(BevelBorder.RAISED), UIMessages.getString("CreditActionDialog.Option.Title.StateOfPlayer"))); //$NON-NLS-1$
			mainPanel.add(statePlayerPanel, new GridBagConstraints(0, y++, 2, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(10, 2, 10, 2), 0, 0));
			ButtonGroup actionButtonGroup = new ButtonGroup();
			actionButtonGroup.add(cannotPayRB);
			actionButtonGroup.add(bankruptRB);
			actionButtonGroup.add(prisonRB);
			cannotPayRB.setSelected(true);
			mEventType = EventType.CANNOT_PAY;
		}
		final JPanel buttonsPanel = new JPanel(new FlowLayout());
		final JButton addButton = new JButton(UIMessages.getString("CreditActionDialog.Button.Label.Ok")); //$NON-NLS-1$
		buttonsPanel.add(addButton);
		addButton.addActionListener(new ApplyAction());
		final JButton cancelButton = new JButton(UIMessageKeyProvider.DIALOG_BUTTON_CANCEL.getMessage());
		buttonsPanel.add(cancelButton);
		cancelButton.addActionListener(new CancelAction());
		mainPanel.add(buttonsPanel, new GridBagConstraints(0, 10, 2, 1, 1, 0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(10, 0, 0, 0), 10, 0));
		getContentPane().add(mainPanel);
		addWindowFocusListener(new WindowFocusListener()
		{
			@Override
			public void windowLostFocus(WindowEvent pE) {}
			
			@Override
			public void windowGainedFocus(WindowEvent pE)
			{
				if (mComponentWithFocus != null)
					mComponentWithFocus.requestFocusInWindow();
			}
		});
	}

	private JRadioButton createRadio(final JPanel pMainPanel, final int pGridy, final String pMessage, final String pActionCommand, final ActionListener pActionListener)
	{
		final JRadioButton radioButton = new JRadioButton(pMessage);
		radioButton.setActionCommand(pActionCommand);
		radioButton.addActionListener(pActionListener);
		pMainPanel.add(radioButton, new GridBagConstraints(0, pGridy, 2, 1, 1, 0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(0, 10, 0, 0), 10, 0));
		radioButton.getInputMap().put(KeyStroke.getKeyStroke((char)10), PlayerEventActionDialog.ENTER_ACTION);
		radioButton.getActionMap().put(PlayerEventActionDialog.ENTER_ACTION, new ApplyAction());
		radioButton.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), PlayerEventActionDialog.ESCAPE_ACTION);
		radioButton.getActionMap().put(PlayerEventActionDialog.ESCAPE_ACTION, new CancelAction());
		return radioButton;
	}

	public JTextField createField(final JPanel pMainPanel, final int pGridy, String pLabel)
	{
		pMainPanel.add(new JLabel(pLabel), new GridBagConstraints(0, pGridy, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 10, 0));
		final JTextField jTextField = new JTextField(50);
		jTextField.setText("0"); //$NON-NLS-1$
		pMainPanel.add(jTextField, new GridBagConstraints(1, pGridy, 1, 1, 1, 0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 10), 10, 0));
		jTextField.getInputMap().put(KeyStroke.getKeyStroke((char)10), PlayerEventActionDialog.ENTER_ACTION);
		jTextField.getActionMap().put(PlayerEventActionDialog.ENTER_ACTION, new ApplyAction());
		jTextField.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), PlayerEventActionDialog.ESCAPE_ACTION);
		jTextField.getActionMap().put(PlayerEventActionDialog.ESCAPE_ACTION, new CancelAction());
		jTextField.addFocusListener(new FocusListener()
		{
			@Override
			public void focusLost(FocusEvent pEvent)
			{
			}
			
			@Override
			public void focusGained(FocusEvent pEvent)
			{
				jTextField.setSelectionStart(0);
				jTextField.setSelectionEnd(jTextField.getText().length());
			}
		});
		return jTextField;
	}

	public int getPrincipal()
	{
		return mPrincipal;
	}

	public int getInterest()
	{
		return mInterest;
	}

	public int getWeakCards()
	{
		return mWeakCards;
	}

	public int getMediumCards()
	{
		return mMediumCards;
	}

	public int getStrongCards()
	{
		return mStrongCards;
	}

	public int getWeakCoins()
	{
		return mWeakCoins;
	}

	public int getMediumCoins()
	{
		return mMediumCoins;
	}

	public int getStrongCoins()
	{
		return mStrongCoins;
	}

	public boolean wasApplied()
	{
		return mApplied;
	}

	public EventType getEventType()
	{
		return mEventType;
	}
}
