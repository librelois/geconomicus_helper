package jyt.geconomicus.helper;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

/**
 * The first dialog that the user sees: he can choose to open an existing game, choose to import a game from a file or create a new game.<br>
 * It then opens the relevant window: the main window or the statistics game chooser.
 * @author jytou
 */
public class EditGameDialog extends JDialog
{
	private static final String OK_ACTION = "ok"; //$NON-NLS-1$
	private static final String CANCEL_ACTION = "cancel"; //$NON-NLS-1$

	// Notify the caller that something has been changed - or not
	private boolean mModified = false;

	public EditGameDialog(final JFrame pParentFrame, final EntityManager pEntityManager, final EntityManagerFactory pEntityManagerFactory, final Game pGame) throws IOException
	{
		super(pParentFrame, UIMessages.getString("EditGameDialog.Title")); //$NON-NLS-1$
		setModal(true);
		final Dimension size = new Dimension(800, 600);
		setSize(size);
		final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(screenSize.width / 2 - size.width/2, screenSize.height / 2 - size.height/2);

		final JPanel mainPanel = new JPanel(new GridBagLayout());

		final GamePropertiesPanel gamePropertiesPanel = new GamePropertiesPanel(pEntityManager, "OK"); //$NON-NLS-1$
		gamePropertiesPanel.setGame(pGame);
		final Action modifyGameAction = new AbstractAction()
		{
			@Override
			public void actionPerformed(ActionEvent pEvent)
			{
				if (gamePropertiesPanel.isChanged())
				{
					EntityManager em = pEntityManager;
					try
					{
						em.getTransaction().begin();
						final int gameType = ((Integer)gamePropertiesPanel.getMoneySystemCB().getSelectedItem()).intValue();
						final Integer nbTurnsPlanned = Integer.valueOf(gamePropertiesPanel.getNbTurnsTextField().getText());
						final Integer moneyCardsFactor = Integer.valueOf(gamePropertiesPanel.getMoneyCardsFactorTextField().getText());
						final double smallCoinValue = NumberFormat.getInstance().parse(gamePropertiesPanel.getSmallCoinValueTextField().getText()).doubleValue();
						if (gameType != pGame.getMoneySystem())
						{
							mModified = true;
							pGame.setMoneySystem(gameType);
						}
						if (nbTurnsPlanned != pGame.getNbTurnsPlanned())
						{
							mModified = true;
							pGame.setNbTurnsPlanned(nbTurnsPlanned);
						}
						if (moneyCardsFactor != pGame.getMoneyCardsFactor())
						{
							mModified = true;
							pGame.setMoneyCardsFactor(moneyCardsFactor);
						}
						if ((pGame.getSmallCoinValue() == null) || (smallCoinValue != pGame.getSmallCoinValue()))
						{
							mModified = true;
							pGame.setSmallCoinValue(smallCoinValue);
						}
						if (pGame.isTake1Penalty() != gamePropertiesPanel.getTake1Penalty().isSelected())
						{
							mModified = true;
							pGame.setTake1Penalty(gamePropertiesPanel.getTake1Penalty().isSelected());
						}
						if (pGame.isUseStrongUnits3().booleanValue() != gamePropertiesPanel.getUseLibreMoney3().isSelected())
						{
							mModified = true;
							pGame.setUseStrongUnits3(gamePropertiesPanel.getUseLibreMoney3().isSelected());
						}
						// Don't bother to tell we modified something for parameters that don't really affect the game itself
						pGame.setAnimatorPseudo(gamePropertiesPanel.getAnimatorTextField().getText());
						pGame.setAnimatorEmail(gamePropertiesPanel.getEmailTextField().getText());
						pGame.setDescription(gamePropertiesPanel.getDescriptionTextArea().getText());
						pGame.setCurdate(gamePropertiesPanel.getDateTextField().getText());
						pGame.setLocation(gamePropertiesPanel.getLocationTextField().getText());
						// If the game was heavily modified, recompute all events
						if (mModified)
							pGame.recomputeAll(null);
						em.getTransaction().commit();
					}
					catch (NumberFormatException | ParseException e)
					{
						em.getTransaction().rollback();
						JOptionPane.showMessageDialog(EditGameDialog.this, UIMessages.getString("ChooseGameDialog.ShouldBeANumber.Error.Message"), UIMessageKeyProvider.DIALOG_TITLE_ERROR.getMessage(), JOptionPane.ERROR_MESSAGE);; //$NON-NLS-1$
						return;
					}
				}
				setVisible(false);
			}
		};
		gamePropertiesPanel.getApplyButton().setMnemonic(UIMessages.getString("ChooseGameDialog.NewGame.Button.Mnemonic").charAt(0)); //$NON-NLS-1$
		gamePropertiesPanel.getApplyButton().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK), EditGameDialog.OK_ACTION);
		gamePropertiesPanel.getApplyButton().getActionMap().put(EditGameDialog.OK_ACTION, modifyGameAction);
		gamePropertiesPanel.getApplyButton().addActionListener(modifyGameAction);
		mainPanel.add(gamePropertiesPanel, new GridBagConstraints(0, 10, 2, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 5, 5));
		gamePropertiesPanel.getMoneySystemCB().addActionListener(new ActionListener()
		{
			private boolean mEditing = false;
			@Override
			public void actionPerformed(ActionEvent pEvent)
			{
				if ((!mEditing) && (JOptionPane.showConfirmDialog(EditGameDialog.this, UIMessages.getString("EditGameDialog.ReallyModifyGameType.Message"), UIMessages.getString("EditGameDialog.ReallyModifyGameType.Title"), JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.NO_OPTION)) //$NON-NLS-1$ //$NON-NLS-2$
				{
					mEditing = true;
					gamePropertiesPanel.getMoneySystemCB().setSelectedItem(pGame.getMoneySystem());
					mEditing = false;
				}
			}
		});

		final Action cancelAction = new AbstractAction()
		{
			@Override
			public void actionPerformed(ActionEvent pEvent)
			{
				if (gamePropertiesPanel.isChanged())
				{
					if (JOptionPane.showConfirmDialog(EditGameDialog.this, UIMessages.getString("EditGameDialog.ReallyCancel.Message"), UIMessages.getString("ChooseGameDialog.ReallyExit.Title"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) //$NON-NLS-1$ //$NON-NLS-2$
						setVisible(false);
				}
				else
					setVisible(false);
			}
		};
		mainPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), EditGameDialog.CANCEL_ACTION);
		mainPanel.getActionMap().put(EditGameDialog.CANCEL_ACTION, cancelAction);

		getContentPane().add(mainPanel);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

	public boolean isModified()
	{
		return mModified;
	}
}
